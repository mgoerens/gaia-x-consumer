package main

import (
    "io/ioutil"
    "net/http"
    "log"
    "os"
    "fmt"
    "github.com/spf13/cobra"
)



var (
    // Used for flags.
    machineId string
    sensorId string
    topic string
    startTime int
    endTime int
)

var rootCmd = &cobra.Command{
  Use:   "consume",
  Short: "Gaia-X Consumer",
  Run: func(cmd *cobra.Command, args []string) {

    log.Print("machineId:", machineId)
    log.Print("sensorId:", sensorId)
    log.Print("topic:", topic)
    log.Print("startTime:", startTime)
    log.Print("endTime:", endTime)
    
    url := fmt.Sprintf("http://gaia-x-aggregate-git-user2-aggregation.apps.ocp4.keithtenzer.com/sensordata/%s/%s/%s?startDate=%d&endDate=%d", topic, machineId, sensorId, startTime, endTime)

    log.Print("url:", url)

    resp, err := http.Get(url)

    if err != nil {
        log.Fatal(err)
    }

    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        log.Fatal(err)
    }

    log.Print(string(body))
  },
}


func init() {

    rootCmd.Flags().StringVarP(&machineId, "machineId", "m", "", "Machine ID")
    rootCmd.Flags().StringVarP(&sensorId, "sensorId", "s", "", "Sensor ID")
    rootCmd.Flags().StringVarP(&topic, "topic", "t", "", "Topic")
    rootCmd.Flags().IntVarP(&startTime, "startTime", "b", 0, "Start Time")
    rootCmd.Flags().IntVarP(&endTime, "endTime", "e", 0, "End Time")

    rootCmd.MarkFlagRequired("machineId")
    rootCmd.MarkFlagRequired("sensorId")
    rootCmd.MarkFlagRequired("topic")
}

func Execute() {
  if err := rootCmd.Execute(); err != nil {
    fmt.Println(err)
    os.Exit(1)
  }
}

func main() {


    Execute()
}
